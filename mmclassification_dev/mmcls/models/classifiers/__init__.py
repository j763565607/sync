# Copyright (c) OpenMMLab. All rights reserved.
from .base import BaseClassifier
from .image import ImageClassifier
from .multi_modal import CLIPClassifier


__all__ = ['BaseClassifier', 'ImageClassifier', 'CLIPClassifier']
