from mmcv.runner import BaseModule
import torch
import os
import numpy as np
from collections import Counter
import json

from ..builder import OOD
from mmcls.models import build_classifier
from .utils import add_noise

@OOD.register_module()
class PosNeg(BaseModule):
    def __init__(self, classifier, **kwargs):
        super(PosNeg, self).__init__()
        self.local_rank = os.environ['LOCAL_RANK']
        self.classifier = build_classifier(classifier)
        self.classifier.eval()

    def forward(self, **input):
        if "type" in input:
            type = input['type']
            del input['type']
        with torch.no_grad():
            pos, neg = self.classifier(return_loss=False, softmax=False, post_process=False, **input)
            pos_softmax = pos.softmax(dim=-1)
            # values, idx = pos_softmax.topk(1, dim=-1)
            # tmp = torch.gather(pos, 1, idx) - torch.gather(neg, 1, idx)
            # confs = torch.sum(tmp*values, dim=-1)
            confs = torch.sum((pos-neg)*pos_softmax, dim=-1)
        return confs, type



