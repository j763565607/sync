export PYTHONPATH="$PYTHONPATH:$PWD"

python src/wise_ft.py   \
    --eval-datasets=ImageNet  \
    --load=/data/csxjiang/ood_ckpt/clip/zeroshot.pt,/data/csxjiang/ood_ckpt/clip/checkpoint_10.pt  \
    --results-db=results.jsonl  \
    --save=/data/csxjiang/ood_ckpt/clip/wiseft  \
    --data-location=/home/csxjiang/jx/sync/wise-ft/data \
    --alpha 1.0